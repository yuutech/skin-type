let isi = function (elm) {
  document.getElementById("saran-mas").innerHTML +=
    '<div class="col-md-4 mt-4"><div class="row align-items-center"><div class="col-md-3 col-4"><img src="' +
    elm[0] +
    '" class="w-100" alt="Meditasi"/></div><div class="col-md-9 col-8">' +
    elm[1] +
    "</div></div></div>";
};

function OSPW() {
  var data = [
    [
      "img/Yoga png.png",
      "Aktivitas yang mengurangi stress seperti yoga, meditasi, mendengarkan music dll",
    ],
    [
      "img/Hat.png",
      "Kenakan kacamata hitam dan topi untuk mengurangi paparan sinar matahari",
    ],
    [
      "img/peanut 2 png.png",
      "Tingkatkan asupan makanan dan minuman yang bersifat antioksidan seperti blueberry, anggur dan teh",
    ],
    [
      "img/bike png.png",
      "Gym, jogging, bersepeda, yoga dan olahraga lain dengan intensitas sedang",
    ],
    ["img/sleeping png.png", "Cukup tidur dan istirahat"],
  ];
  data.map(isi);
}

function OSPT() {
  var data = [
    [
      "img/Yoga png.png",
      "Aktivitas yang mengurangi stress seperti yoga, meditasi, mendengarkan music dll",
    ],
    [
      "img/Hat.png",
      "Kenakan kacamata hitam dan topi untuk mengurangi paparan sinar matahari",
    ],
    [
      "img/bike png.png",
      "Gym, jogging, bersepeda, yoga dan olahraga lain dengan intensitas sedang",
    ],
    ["img/sleeping png.png", "Cukup tidur dan istirahat"],
  ];

  data.map(isi);
}

function OSNW() {
  var data = [
    [
      "img/Yoga png.png",
      "Aktivitas yang mengurangi stress seperti yoga, meditasi, mendengarkan music dll",
    ],
    [
      "img/Hat.png",
      "Kenakan kacamata hitam dan topi untuk mengurangi paparan sinar matahari",
    ],
    [
      "img/peanut 2 png.png",
      "Tingkatkan asupan makanan dan minuman yang bersifat antioksidan seperti blueberry, anggur dan teh",
    ],
    [
      "img/bike png.png",
      "Gym, jogging, bersepeda, yoga dan olahraga lain dengan intensitas sedang",
    ],
    ["img/sleeping png.png", "Cukup tidur dan istirahat"],
  ];
  data.map(isi);
}

function OSNT() {
  var data = [
    [
      "img/Yoga png.png",
      "Aktivitas yang mengurangi stress seperti yoga, meditasi, mendengarkan music dll",
    ],
    [
      "img/Hat.png",
      "Kenakan kacamata hitam dan topi untuk mengurangi paparan sinar matahari",
    ],
    [
      "img/bike png.png",
      "Gym, jogging, bersepeda, yoga dan olahraga lain dengan intensitas sedang",
    ],
    ["img/sleeping png.png", "Cukup tidur dan istirahat"],
  ];
  data.map(isi);
}

function ORPW() {
  var data = [
    [
      "img/Yoga png.png",
      "Aktivitas yang mengurangi stress seperti yoga, meditasi, mendengarkan music dll",
    ],
    [
      "img/peanut 2 png.png",
      "Tingkatkan asupan makanan dan minuman yang bersifat antioksidan seperti blueberry, anggur dan teh",
    ],
  ];
  data.map(isi);
}

function ORPT() {
  var data = [
    [
      "img/Yoga png.png",
      "Aktivitas yang mengurangi stress seperti yoga, meditasi, mendengarkan music dll",
    ],
  ];
  data.map(isi);
}

function ORNW() {
  var data = [
    [
      "img/Yoga png.png",
      "Aktivitas yang mengurangi stress seperti yoga, meditasi, mendengarkan music dll",
    ],
    [
      "img/Hat.png",
      "Kenakan kacamata hitam dan topi untuk mengurangi paparan sinar matahari",
    ],
    [
      "img/peanut 2 png.png",
      "Tingkatkan asupan makanan dan minuman yang bersifat antioksidan seperti blueberry, anggur dan teh",
    ],
    [
      "img/bike png.png",
      "Gym, jogging, bersepeda, yoga dan olahraga lain dengan intensitas sedang",
    ],
    ["img/sleeping png.png", "Cukup tidur dan istirahat"],
  ];
  data.map(isi);
}

function ORNT() {
  var data = [
    [
      "img/Yoga png.png",
      "Aktivitas yang mengurangi stress seperti yoga, meditasi, mendengarkan music dll",
    ],
  ];
  data.map(isi);
}

function DSPW() {
  var data = [
    [
      "img/Yoga png.png",
      "Aktivitas yang mengurangi stress seperti yoga, meditasi, mendengarkan music dll",
    ],
    [
      "img/peanut 2 png.png",
      "Tingkatkan asupan makanan dan minuman yang bersifat antioksidan seperti blueberry, anggur dan teh",
    ],
    [
      "img/Omega png.png",
      "Perbanyak makanan yang kaya akan omega-3 seperti salmon, tuna dan telur",
    ],
    [
      "img/bike png.png",
      "Gym, jogging, bersepeda, yoga dan olahraga lain dengan intensitas sedang",
    ],
    ["img/sleeping png.png", "Cukup tidur dan istirahat"],
    ["img/Massage 2.png", "Terapi pijat"],
  ];
  data.map(isi);
}

function DSPT() {
  var data = [
    [
      "img/Omega png.png",
      "Perbanyak makanan yang kaya akan omega-3 seperti salmon, tuna dan telur",
    ],
    ["img/Massage 2.png", "Terapi pijat"],
  ];
  data.map(isi);
}

function DSNW() {
  var data = [
    [
      "img/peanut 2 png.png",
      "Tingkatkan asupan makanan dan minuman yang bersifat antioksidan seperti blueberry, anggur dan teh",
    ],
    [
      "img/Omega png.png",
      "Perbanyak makanan yang kaya akan omega-3 seperti salmon, tuna dan telur",
    ],
    ["img/Massage 2.png", "Terapi pijat"],
  ];
  data.map(isi);
}

function DSNT() {
  var data = [
    [
      "img/Omega png.png",
      "Perbanyak makanan yang kaya akan omega-3 seperti salmon, tuna dan telur",
    ],
    ["img/Massage 2.png", "Terapi pijat"],
  ];
  data.map(isi);
}

function DRPW() {
  var data = [
    [
      "img/peanut 2 png.png",
      "Tingkatkan asupan makanan dan minuman yang bersifat antioksidan seperti blueberry, anggur dan teh",
    ],
    [
      "img/Omega png.png",
      "Tingkatkan makanan yang kaya akan omega-3 seperti salmon, tuna dan telur",
    ],
  ];
  data.map(isi);
}

function DRPT() {
  var data = [
    [
      "img/Hat.png",
      "Kenakan kacamata hitam dan topi untuk mengurangi paparan sinar matahari",
    ],
    [
      "img/peanut 2 png.png",
      "Tingkatkan asupan makanan dan minuman yang bersifat antioksidan seperti blueberry, anggur dan teh",
    ],
    [
      "img/bike png.png",
      "Gym, jogging, bersepeda, yoga dan olahraga lain dengan intensitas sedang",
    ],
    ["img/sleeping png.png", "Cukup tidur dan istirahat"],
  ];
  data.map(isi);
}

function DRNW() {
  var data = [
    [
      "img/peanut 2 png.png",
      "Tingkatkan asupan makanan dan minuman yang bersifat antioksidan seperti blueberry, anggur dan teh",
    ],
    [
      "img/Omega png.png",
      "Tingkatkan makanan yang kaya akan omega-3 seperti salmon, tuna dan telur",
    ],
  ];
  data.map(isi);
}

function DRNT() {
  var data = [
    [
      "img/Omega png.png",
      "Tingkatkan makanan yang kaya akan omega-3 seperti salmon, tuna dan telur",
    ],
    [
      "img/bike png.png",
      "Gym, jogging, bersepeda, yoga dan olahraga lain dengan intensitas sedang",
    ],
    ["img/sleeping png.png", "Cukup tidur dan istirahat"],
  ];
  data.map(isi);
}

function ERR() {
  document.getElementById("saran-mas").innerHTML +=
    '<div class="col-md-12 mt-4 text-center">Jawab Semua Kuisioner Terlebih dahulu untuk melihat hasilnya</div>';
}
