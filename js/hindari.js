let isi1 = function (elm) {
  document.getElementById("hindari-dek").innerHTML +=
    '<div class="col-md-4 mt-4"><div class="row align-items-center"><div class="col-md-3 col-4"><img src="' +
    elm[0] +
    '" class="w-100" alt="Meditasi"/></div><div class="col-md-9 col-8">' +
    elm[1] +
    "</div></div></div>";
};

function OSPW1() {
  var data1 = [
    [
      "img/cold png.png",
      "Berada di lingkungan dengan perubahan suhu yang ekstrem",
    ],
    ["img/Keluar siang.png", "Aktivitas di luar ruangan pada tengah hari"],
    ["img/burger png.png", "Makanan berminyak, pedas dan tinggi lemak"],
    ["img/Beer.png", "Makanan cepat saji, kopi, alkohol dan merokok"],
    ["img/Sugar.png", "Makanan dengan kadar gula tinggi"],
    [
      "img/flask png.png",
      "Kontak langsung dengan pewarna kimia, pengelupas kulit dan bahan kimia lainnya",
    ],
    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
    ["img/Sun png.png", "Paparan sinar matahari dalam waktu lama"],
  ];
  data1.map(isi1);
}

function OSPT1() {
  var data1 = [
    [
      "img/cold png.png",
      "Berada di lingkungan dengan perubahan suhu yang ekstrem",
    ],
    ["img/Keluar siang.png", "Aktivitas di luar ruangan pada tengah hari"],
    ["img/burger png.png", "Makanan berminyak, pedas dan tinggi lemak"],
    ["img/Beer.png", "Makanan cepat saji, kopi, alkohol dan merokok"],
    ["img/Sugar.png", "Makanan dengan kadar gula tinggi"],
    [
      "img/flask png.png",
      "Kontak langsung dengan pewarna kimia, pengelupas kulit dan bahan kimia lainnya",
    ],
    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
  ];

  data1.map(isi1);
}

function OSNW1() {
  var data1 = [
    [
      "img/cold png.png",
      "Berada di lingkungan dengan perubahan suhu yang ekstrem",
    ],
    ["img/Keluar siang.png", "Aktivitas di luar ruangan pada tengah hari"],
    ["img/burger png.png", "Makanan berminyak, pedas dan tinggi lemak"],
    ["img/Beer.png", "Makanan cepat saji, kopi, alkohol dan merokok"],
    ["img/Sugar.png", "Makanan dengan kadar gula tinggi"],
    [
      "img/flask png.png",
      "Kontak langsung dengan pewarna kimia, pengelupas kulit dan bahan kimia lainnya",
    ],
    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
  ];
  data1.map(isi1);
}

function OSNT1() {
  var data1 = [
    [
      "img/cold png.png",
      "Berada di lingkungan dengan perubahan suhu yang ekstrem",
    ],
    ["img/Keluar siang.png", "Aktivitas di luar ruangan pada tengah hari"],
    ["img/burger png.png", "Makanan berminyak, pedas dan tinggi lemak"],
    ["img/Beer.png", "Makanan cepat saji, kopi, alkohol dan merokok"],
    ["img/Sugar.png", "Makanan dengan kadar gula tinggi"],
    [
      "img/flask png.png",
      "Kontak langsung dengan pewarna kimia, pengelupas kulit dan bahan kimia lainnya",
    ],
  ];
  data1.map(isi1);
}

function ORPW1() {
  var data1 = [
    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
    ["img/Sun png.png", "Paparan sinar matahari dalam waktu lama"],
  ];
  data1.map(isi1);
}

function ORPT1() {
  var data1 = [
    ["img/burger png.png", "Makanan berminyak, pedas dan tinggi lemak"],
    ["img/Sun png.png", "Paparan sinar matahari dalam waktu lama"],
  ];
  data1.map(isi1);
}

function ORNW1() {
  var data1 = [
    ["img/Keluar siang.png", "Aktivitas di luar ruangan pada tengah hari"],
    ["img/burger png.png", "Makanan berminyak, pedas dan tinggi lemak"],
    ["img/Beer.png", "Makanan cepat saji, kopi, alkohol dan merokok"],
    ["img/Sugar.png", "Makanan dengan kadar gula tinggi"],

    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
  ];
  data1.map(isi1);
}

function ORNT1() {
  var data1 = [["img/Insomnia png.png", "Terjaga hingga larut malam"]];
  data1.map(isi1);
}

function DSPW1() {
  var data1 = [
    [
      "img/fast-food png.png",
      "Makanan yang kaya akan omega-6 seperti makanan cepat saji, kentang goreng, kue dan permen",
    ],
    ["img/burger png.png", "Makanan berminyak, pedas dan tinggi lemak"],
    ["img/Beer.png", "Makanan cepat saji, kopi, alkohol dan merokok"],
    ["img/Sugar.png", "Makanan dengan kadar gula tinggi"],
    [
      "img/flask png.png",
      "Kontak langsung dengan pewarna kimia, pengelupas kulit dan bahan kimia lainnya",
    ],
    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
    ["img/Sauna.png", "Sauna atau steam"],
  ];
  data1.map(isi1);
}

function DSPT1() {
  var data1 = [
    [
      "img/fast-food png.png",
      "Makanan yang kaya akan omega-6 seperti makanan cepat saji, kentang goreng, kue dan permen",
    ],
    [
      "img/flask png.png",
      "Kontak langsung dengan pewarna kimia, pengelupas kulit dan bahan kimia lainnya",
    ],
    ["img/Sun png.png", "Paparan sinar matahari dalam waktu lama"],
  ];
  data1.map(isi1);
}

function DSNW1() {
  var data1 = [
    [
      "img/fast-food png.png",
      "Makanan yang kaya akan omega-6 seperti makanan cepat saji, kentang goreng, kue dan permen",
    ],
    [
      "img/flask png.png",
      "Kontak langsung dengan pewarna kimia, pengelupas kulit dan bahan kimia lainnya",
    ],
    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
    ["img/Sun png.png", "Paparan sinar matahari dalam waktu lama"],
  ];
  data1.map(isi1);
}

function DSNT1() {
  var data1 = [
    [
      "img/fast-food png.png",
      "Makanan yang kaya akan omega-6 seperti makanan cepat saji, kentang goreng, kue dan permen",
    ],
    [
      "img/flask png.png",
      "Kontak langsung dengan pewarna kimia, pengelupas kulit dan bahan kimia lainnya",
    ],
    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
    ["img/Sun png.png", "Paparan sinar matahari dalam waktu lama"],
  ];
  data1.map(isi1);
}

function DRPW1() {
  var data1 = [
    [
      "img/fast-food png.png",
      "Makanan yang kaya akan omega-6 seperti makanan cepat saji, kentang goreng, kue dan permen",
    ],

    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
    ["img/Sun png.png", "Paparan sinar matahari dalam waktu lama"],
  ];
  data1.map(isi1);
}

function DRPT1() {
  var data1 = [
    [
      "img/fast-food png.png",
      "Makanan yang kaya akan omega-6 seperti makanan cepat saji, kentang goreng, kue dan permen",
    ],
    ["img/Keluar siang.png", "Aktivitas di luar ruangan pada tengah hari"],
    ["img/burger png.png", "Makanan berminyak, pedas dan tinggi lemak"],
    ["img/Beer.png", "Makanan cepat saji, kopi, alkohol dan merokok"],
    ["img/Sugar.png", "Makanan dengan kadar gula tinggi"],
  ];
  data1.map(isi1);
}

function DRNW1() {
  var data1 = [
    [
      "img/fast-food png.png",
      "Makanan yang kaya akan omega-6 seperti makanan cepat saji, kentang goreng, kue dan permen",
    ],
    ["img/Insomnia png.png", "Terjaga hingga larut malam"],
  ];
  data1.map(isi1);
}

function DRNT1() {
  var data1 = [
    [
      "img/fast-food png.png",
      "Makanan yang kaya akan omega-6 seperti makanan cepat saji, kentang goreng, kue dan permen",
    ],
  ];
  data1.map(isi1);
}

function ERR1() {
  document.getElementById("hindari-dek").innerHTML +=
    '<div class="col-md-12 mt-4 text-center">Jawab Semua Kuisioner Terlebih dahulu untuk melihat hasilnya</div>';
}
