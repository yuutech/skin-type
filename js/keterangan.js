function keterangan() {
  if (d1 == "O" && d2 == "S" && d3 == "P" && d4 == "W") {
    OSPW();
    OSPW1();
    document.getElementById("saran").innerHTML =
      "Kulit kamu menunjukkan kondisi yang berminyak sehingga gampang banget berjerawat dan sensitif saat terkena debu atau make up sehingga menyebabkan ruam atau iritasi kulit. Lalu, karena kulit kamu berpigmen, masalah dari iritasi akan menyebabkan penggelapan kulit terutama di daerah yang berjerawat, atau biasa disebut sebagai bekas jerawat. Tipe kulit kamu ini memiliki kecenderungan untuk lebih mudah berkerut karena gaya hidup. Terlebih, bila aktivitas sering dilakukan di luar ruangan sehingga terpapar sinar matahari dan merokok";
  } else if (d1 == "O" && d2 == "S" && d3 == "P" && d4 == "T") {
    OSPT();
    OSPT1();
    document.getElementById("saran").innerHTML =
      "Kulit wajah kamu cenderung lebih berminyak, sensitif dan penggelapan kulit di daerah jerawat atau iritasi. Bercak gelap seperti melasma juga akan muncul. Namun, jenis kulitmu ini memiliki kesempatan lebih rendah untuk berkeriput. Gaya hidup yang sehat serta mengurangi paparan polusi akan membantu mencegah keriput tersebut.";
  } else if (d1 == "O" && d2 == "S" && d3 == "N" && d4 == "W") {
    OSNW();
    OSNW1();
    document.getElementById("saran").innerHTML =
      "Masalah yang sering terjadi pada kulit kamu ini terutama mengenai produksi minyak dan sensitivitasnya. Hanya saja, kulit dengan karakter ini akan lebih mudah berkeriput karena tidak memiliki kadar pigmen kulit yang berfungsi untuk melindungi kulit.";
  } else if (d1 == "0" && d2 == "S" && d3 == "N" && d4 == "T") {
    OSNT();
    OSNT1();
    document.getElementById("saran").innerHTML =
      "Jenis kulit kamu ini dapat dikatakan sebagai salah satu yang beruntung. Meskipun memiliki kondisi kulit berminyak, sensitif dan tidak berpigmen, kondisi kulit kamu juga memiliki kecenderungan lebih rendah untuk keriput. Hal ini biasanya terpengaruh dari DNA yang dimiliki tiap individu. Perawatan kulit untuk jenis ini juga akan lebih mudah.";
  } else if (d1 == "O" && d2 == "R" && d3 == "P" && d4 == "W") {
    ORPW();
    ORPW1();
    document.getElementById("saran").innerHTML =
      "Meskipun berminyak, kulit kamu akan jarang mengalami masalah jerawat atau ruam. Namun, bercak gelap akan tetap terjadi. Kulit kamu termasuk memiliki tingkat resisten yang tinggi sehingga penggunaan produk bahan aktif yang lebih tinggi  masih dianjurkan. ";
  } else if (d1 == "O" && d2 == "R" && d3 == "P" && d4 == "T") {
    ORPT();
    ORPT1();
    document.getElementById("saran").innerHTML =
      "Masalah yang sering terjadi pada jenis kulit kamu ini yaitu munculnya flek hitam. Namun, karena pigmen yang ada pula, kecenderungan untuk mengalami penuaan atau keriput juga lebih kecil. Hal ini bisa didapatkan pula dengan gaya hidup yang baik, seperti menghindari paparan sinar matahari dan merokok.";
  } else if (d1 == "O" && d2 == "R" && d3 == "N" && d4 == "W") {
    ORNW();
    ORNW1();
    document.getElementById("saran").innerHTML =
      "Masalah yang sering terjadi pada jenis kulit kamu ini yaitu ada pada bagian produksi minyak. Hanya saja, kulit dengan karakter ini akan jarang berjerawat tapi lebih mudah keriput karena pigmentasi kulit yang kurang melindungi.";
  } else if (d1 == "O" && d2 == "R" && d3 == "N" && d4 == "T") {
    ORNT();
    ORNT1();
    document.getElementById("saran").innerHTML =
      "Bila kamu memiliki jenis ini kulit ini, masalah yang sering muncul hanya seputar kulit berminyak dan muncul flek hitam. Tapi, kamu tidak akan berjerawat dan garis-garis wajah tidak akan tampak, misal saat sedang berkerut atau tersenyum.";
  } else if (d1 == "D" && d2 == "S" && d3 == "P" && d4 == "W") {
    DSPW();
    DSPW1();
    document.getElementById("saran").innerHTML =
      "Ciri-ciri dari jenis ini kulit kamu ini adalah, meskipun kulitmu kering tapi kerap berjerawat. Selain itu, bercak atau bintik hitam juga akan lebih sering terlihat, terlebih bila sering terkena paparan sinar matahari.";
  } else if (d1 == "D" && d2 == "S" && d3 == "P" && d4 == "T") {
    DSPT();
    DSPT1();
    document.getElementById("saran").innerHTML =
      "Bila kamu sering mengalami ruam kulit dan dehidrasi, itu menandakan kamu memiliki karakter kulit jenis DSPT. Hal tersebut dikarenakan lapisan pelindung kulitmu yang lemah sehingga membuat kulitmu lebih mudah berjerawat. Namun, kulit  kamu akan mengalami kecenderungan lebih rendah untuk berkeriput yang dapat terlihat pada muncul atau tidaknya garis halus di wajah.";
  } else if (d1 == "D" && d2 == "S" && d3 == "N" && d4 == "W") {
    DSNW();
    DSNW1();
    document.getElementById("saran").innerHTML =
      "Masalah yang sering terjadi pada kulit kamu ini yakni lemahnya lapisan pelindung kulit. Pigmentasi kulitmu juga lebih rendah sehingga kesempatan untuk munculnya keriput lebih tinggi. Biasanya, kebiasaan buruk seperti merokok atau beraktivitas di luar ruangan tanpa pelindung juga akan meningkatkan risiko tersebut.";
  } else if (d1 == "D" && d2 == "S" && d3 == "N" && d4 == "T") {
    DSNT();
    DSNT1();
    document.getElementById("saran").innerHTML =
      "Meskipun kulit kamu kering, tapi kamu tidak memiliki garis halus di wajah saat tersenyum atau berkerut dan tidak ada flek hitam. Masalah kulit yang sering kamu alami hanya seputar jerawat saja, bahkan bekas jerawatnya pun jarang terlihat.";
  } else if (d1 == "D" && d2 == "R" && d3 == "P" && d4 == "W") {
    DRPW();
    DRPW1();
    document.getElementById("saran").innerHTML =
      "Jenis kulit kamu ini akan membuat kamu jarang memiliki jerawat, ruam atau kemerahan. Namun, bercak gelap akan sering terjadi sehingga kamu harus rajin-rajin mengecek wajah kamu dengan teliti.";
  } else if (d1 == "D" && d2 == "R" && d3 == "P" && d4 == "T") {
    DRPT();
    DRPT1();
    document.getElementById("saran").innerHTML =
      "Dapat dikatakan, jenis kulit kamu ini hanya memiliki dua masalah, yakni kulit kering dan muncul flek hitam. Bila kamu jarang berjerawat dan iritasi, serta kulit terasa kencang, maka kamu memang memiliki tipe DRPT ini.";
  } else if (d1 == "D" && d2 == "R" && d3 == "N" && d4 == "W") {
    DRNW();
    DRNW1();
    document.getElementById("saran").innerHTML =
      "Apakah kulit kamu muncul garis halus dan keriput di usia muda?Artinya kulit kamu memang memiliki jenis kulit DRNW. Hal ini dikarenakan kondisi kulit kamu yang kering, ditambah dengan kadar pigmennya kurang sehingga memiliki kecenderungan untuk berkerut. Tapi, kamu tidak akan mengalami masalah jerawat.";
  } else if (d1 == "D" && d2 == "R" && d3 == "N" && d4 == "T") {
    DRNT();
    DRNT1();
    document.getElementById("saran").innerHTML =
      "Kulit kamu tidak sensitif, tidak ada flek dan terasa kencang, namun kering. Yang kamu butuhkan tentunya pelembap wajah sehingga kulitmu lebih terhidrasi. Lalu, hindari pula kebiasaan buruk seperti merokok, dan terus gunakan tabir surya / sunscreen saat terkena sinar matahari";
  } else {
    ERR();
    ERR1();
  }
}
